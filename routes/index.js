var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'DEMO CHAT' });
});
router.get('/chat1', function(req, res, next) {
  res.render('chat1', { title: 'Chat1' });
});
router.get('/chat2', function(req, res, next) {
  res.render('chat2', { title: 'Chat2' });
});



module.exports = router;
