CREATE TABLE `messages2` (
  `id` int(11) primary key auto_increment,
  `userId` int(11) NOT NULL,
  `toId` int(11) NOT NULL,
  `msg` mediumtext COLLATE utf8mb4_bin NOT NULL,
  `type` int(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 / null = text, 1 = image, 2=video',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `read_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;